- **配置环境变量**
  
  - 使用[MySQL镜像](https://hub.docker.com/_/mysql)创建Pod，需要使用环境变量设置MySQL的初始密码。
  - [环境变量配置示例](https://kubernetes.io/zh-cn/docs/tasks/inject-data-application/define-environment-variable-container/#define-an-env-variable-for-a-container)
- **挂载卷**
  
  - 将数据存储在容器中，一旦容器被删除，数据也会被删除。
  - 将数据存储到**卷(Volume)** 中，删除容器时，卷不会被删除。

---

#### hostPath卷

**hostPath**卷将主机节点上的文件或目录挂载到 Pod 中。
[hostPath配置示例](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath-configuration-example)

---

**hostPath的type值：**

| **DirectoryOrCreate** | **目录不存在则自动创建。** |
| --- | --- |
| **Directory** | **挂载已存在目录。不存在会报错。** |
| **FileOrCreate** | **文件不存在则自动创建。**
**不会自动创建文件的父目录，必须确保文件路径已经存在。** |
| **File** | **挂载已存在的文件。不存在会报错。** |
| **Socket** | **挂载 UNIX 套接字。例如挂载/var/run/docker.sock进程** |

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mysql-pod
spec:
  containers:
    - name: mysql
      image: mysql:5.7
      env:
        - name: MYSQL_ROOT_PASSWORD
          value: "123456"
      ports:
        - containerPort: 3306
      volumeMounts:
        - mountPath: /var/lib/mysql #容器中的目录
          name: data-volume
  volumes:
    - name: data-volume
      hostPath:
        # 宿主机上目录位置
        path: /home/mysql/data
        type: DirectoryOrCreate
```

> **注意：hostPath** 仅用于在单节点集群上进行开发和测试，不适用于多节点集群；
> 例如，当Pod被重新创建时，可能会被调度到与原先不同的节点上，导致新的Pod没有数据。
> 在多节点集群使用本地存储，可以使用`local`卷。

---

参考文档：
[https://kubernetes.io/zh-cn/docs/tasks/inject-data-application/define-environment-variable-container/](https://kubernetes.io/zh-cn/docs/tasks/inject-data-application/define-environment-variable-container/)
[https://kubernetes.io/zh-cn/docs/concepts/storage/volumes/#hostpath](https://kubernetes.io/zh-cn/docs/concepts/storage/volumes/#hostpath)
[https://kubernetes.io/zh-cn/docs/tasks/configure-pod-container/configure-persistent-volume-storage/](https://kubernetes.io/zh-cn/docs/tasks/configure-pod-container/configure-persistent-volume-storage/)

